

## ONex B2B2C 二次开发 程序目录结构

### 主要app 介绍

app				用途

shopex			订单，商品，会员
ectools			电商基础工具
pam				登录认证
dbeav			数据库扩展
search			分词+索引
site			站点
desktop			后台操作界面
image			图片存取
setup			系统安装工具
base			基础MVC

### ECOS 目录结构
|- app			应用程序目录
|- bootstrap	
|- config		配置文件目录
|- data			数据文件（kvstore，缓存）
|- demo			初始化数据文件目录
|- misc			开发者认证相关
|- public		商城前台
|- script		扩展
|- vendor		库文件
|- index.php	应用程序入口文件
|- license.txt	OCOS软件协议文件


### ECOS Widgets 构成
|- _config.html 挂件参数设置模板，在后台可视编辑中使用的参数设置界面；
|- default.html 挂件前台展现模板，可以有多个，在后台参数设置时切换；
|- widget_挂件名.php 挂件的主要代码，获取数据，实现功能逻辑；
|- widget.php 挂件的定义和默认参数配置文件。
|- images	对应挂件视觉效果的icon.jpg、widget.jpg 仅允许png/jpg/gif三种形式的图片文件。

### ONexB2B2C常用APP目录

APP 常用目录
|- sysshop					店铺中心系统
|- topc						客户前端系统
|- topm						客户移动端系统
|- topshop					店铺业务管理
|- toputil					店铺通用显示数据处理

常用模块

systrade订单处理模块，创建订单/加入购物车等都在这里
sysuser 用户管理模块，只要需要用到和用户/商户/管理员相关的都在这里
sysshop 后台商家信息，商家列表/维护管理等
sysitem 商品信息，产品信息/sku商品信息，只要有关商品的都在这里
syscategory 类目信息，平台的分类等管理的地方

前台就说两个吧
topm/topc 前台显示的手机端和PC电脑端，默认入口 都是default
topshop 前台商家管理界面，管理店铺，商家商品管理，订单处理，交易统计等