

## ONex B2B2C 二次开发之常用函数

### 0.1 转义用户输入
`$keyword = utils::_filter_input(input::get('keyword'));`

### 0.2 action跳转
`return redirect::action('topc_ctl_shopcenter@search',array('n'=>$keyword));`

退回

`return redirect::back();`

注：
 array 内为`get`方式参数。
 
 ### 0.3 splash 消息提示
 `return $this->splash('success',$url,$msg,true)`
 
demo
``` `php
$url = url::action('topc_ctl_item@index',array('item_id'=>$postdata['item_id']));

$msg = '咨询提交成功,请耐心等待商家审核、回复';
return $this->splash('success',$url,$msg,true);
```

### 0.4 