## ONex B2B2C 二次开发之querybuilder的使用


### 前言 

ecstore中的 querybuilder 是继承自 Doctrine 。  
QueryBuilder是doctrine提供的一种在DQL之上的一层查询操作，它封装了一些api，提供给用户进行组装DQL的。

QueryBuilder的好处就是看起来不用自己字符串拼装查询语句了。

### 位置-

`/webroot/vendor/doctrine/dbal/lib/Doctrine/DBAL/Query/QueryBuilder.php`

### 调用方式

``` php
        $qb = app::get('sysitem')->database()->createQueryBuilder();
        $qb->select('item_id')
            ->from('sysitem_item_nature_props')
            ->where($objMdlItemNatureProps->_filter($propIndexFilter))
            ->groupBy('item_id')
            ->having('count(item_id)>='.count($count));

        $data = $qb->execute()->fetchAll();
```

### 方法

#### 0.1 select

``` php
        $qb->select('a.chexing_id,a.chexing_name,b.chexi_name,c.nianfen_name,d.pailiang_name')
            ->from('chexing_chexing','a')
            ->where($filter)
            ->leftJoin('a','chexing_chexi','b','a.brand_id = b.brand_id')
            ->leftJoin('a','chexing_nianfen','c','a.nianfen_id = c.nianfen_id')
            ->leftJoin('a','chexing_pailiang','d','a.pailiang_id = d.pailiang_id')
            ->groupby('a.chexing_id');
        $result = $qb->execute()->fetch(); // 一维数组
        $result = $qb->execute()->fetchAll(); //二维数组
```

#### 0.2 from

#### 0.3 leftJoin

#### 0.4 set

#### 0.5 where

#### 0.6 groupBy
#### 0.7 having
#### 0.8 orderBy
#### 0.9 values

#### 1.0 fetchAll

#### 1.1 fetch

#### 1.2 insert

``` php
$qb->insert('mini_addrs')
  ->values(array(
  	'id' => 1,
    'user_id' => 3,
  ));
//->setValue(
//	'display' => false,
//)
  try{
    $qb->execute();
  }
  // 主键重
  catch (UniqueConstraintViolationException $e) 
  {
    return false;
  }
  $inserId = $this->lastInsertId();

  return isset($inserId) ? $inserId : true;
```










